import numpy as np
import torch
import matplotlib as mpl
import matplotlib.pyplot as plt
import random
from scipy.stats import wasserstein_distance


import scipy.spatial.distance as dist
from scipy import stats

font = {'family' : 'serif',
        'size'   : 18}
mpl.rc('font', **font)
plt.style.use('classic')
mpl.rc('font', **font)

def getTotE(data, xbins, ybins, layers):
    data = np.reshape(data,[-1, layers*xbins*ybins])
    etot_arr = np.sum(data, axis=(1))
    return etot_arr

def plot_image2D(image):
    cmap = mpl.cm.viridis
    cmap.set_bad('white',1.)
    #figExIm = plt.figure(figsize=(6,6))
    #axExIm1 = figExIm.add_subplot(1,1,1)
    
    figExIm, (axExIm1, axExIm2, axExIm3) = plt.subplots(1, 3, figsize=(25, 6))
    
    
    r = random.randrange(1, 100)

    
    ## Y-Z
    image1 = np.sum(image[r].numpy(), axis=0)#+1.0
    im1 = axExIm1.imshow(image1, filternorm=False, interpolation='none', cmap = cmap, vmin=0.01, vmax=100,
                       norm=mpl.colors.LogNorm(), origin='lower')
    figExIm.patch.set_facecolor('white')
    axExIm1.set_xlabel('y [cells]', family='serif')
    axExIm1.set_ylabel('x [cells]', family='serif')
    figExIm.colorbar(im1)
    
    # Y-Z
    image2 = np.sum(image[r].numpy(), axis=1)#+1.0
    im2 = axExIm2.imshow(image2, filternorm=False, interpolation='none', cmap = cmap, vmin=0.01, vmax=100,
                       norm=mpl.colors.LogNorm(), origin='lower')
    figExIm.patch.set_facecolor('white')  
    axExIm2.set_xlabel('y [cells]', family='serif')
    axExIm2.set_ylabel('z [layers]', family='serif')
    #figExIm.colorbar(im2)
    
    # Z-X
    image3 = np.sum(image[r].numpy(), axis=2)#+1.0
    im3 = axExIm3.imshow(image3, filternorm=False, interpolation='none', cmap = cmap, vmin=0.01, vmax=100,
                       norm=mpl.colors.LogNorm(), origin='lower')
    figExIm.patch.set_facecolor('white')  
    axExIm3.set_xlabel('x [cells]', family='serif')
    axExIm3.set_ylabel('z [layers]', family='serif')
    #figExIm.colorbar(im3)

    
def jsdHist(data_real, data_fake, nbins, minE, maxE, eph, debug=False):
    
    figSE = plt.figure(figsize=(6,6*0.77/0.67))
    axSE = figSE.add_subplot(1,1,1)


    pSEa = axSE.hist(data_real, bins=nbins, 
            weights=np.ones_like(data_real)/(float(len(data_real))), 
            histtype='step', color='black',
            range=[minE, maxE])
    pSEb = axSE.hist(data_fake, bins=nbins, 
            weights=np.ones_like(data_fake)/(float(len(data_fake))),
            histtype='step', color='red',
             range=[minE, maxE])

    frq1 = pSEa[0]
    frq2 = pSEb[0]

    JSD = dist.jensenshannon(frq1, frq2)    

    if (debug):
        plt.savefig('./jsd/esum_'+str(eph)+'.png')

    plt.close()
    
    if len(frq1) != len(frq2):
        print('ERROR JSD: Histogram bins are not matching!!')
    return JSD
    

def wassHist(data_real, data_fake, nbins, minE, maxE, eph, debug=False):
    
    figSE = plt.figure(figsize=(6,6*0.77/0.67))
    axSE = figSE.add_subplot(1,1,1)


    pSEa = axSE.hist(data_real, bins=nbins, 
            weights=np.ones_like(data_real)/(float(len(data_real))), 
            histtype='step', color='black',
            range=[minE, maxE])
    pSEb = axSE.hist(data_fake, bins=nbins, 
            weights=np.ones_like(data_fake)/(float(len(data_fake))),
            histtype='step', color='red',
             range=[minE, maxE])

    frq1 = pSEa[0]
    frq2 = pSEb[0]

    wd = wasserstein_distance(frq1, frq2)    

    if (debug):
        plt.savefig('./jsd/esum_'+str(eph)+'.png')

    plt.close()
    
    if len(frq1) != len(frq2):
        print('ERROR wasserstein distance: Histogram bins are not matching!!')
    return wd
    
    
    