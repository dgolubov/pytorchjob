import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as mpatches
import numpy as np
from matplotlib.colors import LogNorm

font = {'family' : 'serif',
        'size'   : 18}
mpl.rc('font', **font)
plt.style.use('classic')
mpl.rc('font', **font)


def plot_esum(real, fake, nbins, minE, maxE, ymax, name):
    
    
    figSE = plt.figure(figsize=(6,6*0.77/0.67))
    axSE = figSE.add_subplot(1,1,1)


    pSEa = axSE.hist(real, bins=nbins, 
            histtype='stepfilled', color='lightgrey',
            range=[minE, maxE])
    pSEb = axSE.hist(fake, bins=nbins, 
            histtype='step', color='red',
             range=[minE, maxE])

    
    
    axSE.text(0.60, 0.87, 'GEANT 4', horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'grey')
    
    axSE.text(0.60, 0.80, 'WGAN', horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'red')
       
    axSE.text(0.6,
            0.95,
            '50GeV', horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes)

    
    figSE.patch.set_facecolor('white') 
        
    axSE.set_xlim([0, maxE])
    axSE.set_ylim([0, ymax])
    axSE.set_xlabel("MeV", family='serif')
    
    plt.savefig('./esum'+str(name)+'.png')

    
    
def plt_Profile(data_real, data_fake, model_title='ML Model',
                model_title2='ML Model2', save_title='ML_model',
                number=1, numberf=1, numberf2=1,
                save_fig_name="_long_E_dist", y_title = 'layer'):
    
    figSpnE = plt.figure(figsize=(6,6))
    axSpnE = figSpnE.add_subplot(1,1,1)
    lightblue = (0.1, 0.1, 0.9, 0.3)
    n_layers = data_real.shape[1]
    hits = np.arange(0, n_layers)+0.5
    
   
    pSpnEa = axSpnE.hist(hits, bins=n_layers, range=[0,n_layers], density=None, 
                   weights=np.mean(data_real, 0), edgecolor='grey', 
                   label = "orig", linewidth=1, color='lightgrey',
                   histtype='stepfilled')

    #axSpnE.plot((0.42, 0.48),(0.87-0.02, 0.87-0.02),linewidth=1, 
    #         transform=axSpnE.transAxes, color = 'grey') 
    axSpnE.text(0.60, 0.87, 'GEANT 4', horizontalalignment='left',verticalalignment='top', 
             transform=axSpnE.transAxes, color = 'grey')


    
    pSpnEb = axSpnE.hist(hits, bins=pSpnEa[1], range=None, density=None, 
                   weights=np.mean(data_fake, 0), edgecolor='red', 
                   label = "orig", linewidth=1,
                   histtype='step')
    
    #axSpnE.plot((0.42, 0.42),(0.87-0.02, 0.87-0.02), 
    #         transform=axSpnE.transAxes, color = 'red') 
    axSpnE.text(0.60, 0.80, 'WGAN', horizontalalignment='left',verticalalignment='top', 
             transform=axSpnE.transAxes, color = 'red')


    axSpnE.set_xlabel(y_title, family='serif')
    axSpnE.set_ylabel('energy [MeV]', family='serif')
    axSpnE.set_xlim([0, n_layers+1.0])
    axSpnE.set_ylim([1, 100])
    #axSpnE.xaxis.set_ticks([0.5,1.0,1.5,2.0])

    #axSpnE.xaxis.set_minor_locator(MultipleLocator(1))
    #axSpnE.xaxis.set_major_locator(MultipleLocator(5))

    axSpnE.text(0.6,
            0.95,
            '50GeV', horizontalalignment='left',verticalalignment='top', 
             transform=axSpnE.transAxes)

    plt.subplots_adjust(left=0.18, right=0.95, top=0.95, bottom=0.18)
    plt.yscale('log')
    figSpnE.patch.set_facecolor('white')

    plt.savefig('./plot_' + save_title + save_fig_name+ ".png")

    
def plt_nhits(data_real, data_fake, model_title='ML Model', model_title2='ML Model 2', save_title=' '):
    #figOcc = plt.figure(figsize=(6,6))
    figOcc = plt.figure(figsize=(6,6*0.77/0.67))

    axOcc = figOcc.add_subplot(1,1,1)
    lightblue = (0.1, 0.1, 0.9, 0.3)

    xmin = 0
    xmax = 1000
    nbins= 50
    ymax = 0.15
    ymin = 0
    
    

    pOcca = axOcc.hist(data_real, bins=nbins, range=[xmin,xmax], density=None, 
                   weights=np.ones_like(data_real)/(float(len(data_real))), edgecolor='black', linewidth=1,
                   color='lightgrey',
                   histtype='stepfilled')
    pOccb = axOcc.hist(data_fake, bins=pOcca[1], range=None, density=None, 
                   weights=np.ones_like(data_fake)/(float(len(data_fake))), linewidth=1, edgecolor='red',
                   histtype='step')
    
    
    axOcc.text(0.50, 0.81, model_title, horizontalalignment='left',verticalalignment='top', 
             transform=axOcc.transAxes, color = 'red')
    axOcc.text(0.50, 0.87, 'GEANT 4', horizontalalignment='left',verticalalignment='top', 
             transform=axOcc.transAxes, color = 'grey')


   


    axOcc.set_xlabel('number of hits', family='serif')
    axOcc.set_ylabel('a.u.', family='serif')
    axOcc.set_xlim([xmin, xmax])
    axOcc.set_ylim([ymin, ymax])
    axOcc.text(0.5, 0.95,save_title, horizontalalignment='left',verticalalignment='top', 
                 transform=axOcc.transAxes)

    #axOcc.xaxis.set_minor_locator(MultipleLocator(50))
    #axOcc.xaxis.set_major_locator(MultipleLocator(200))
  
    
    #plt.subplots_adjust(left=0.18, right=0.95, top=0.95, bottom=0.18)
    plt.subplots_adjust(left=0.18, right=0.95, top=0.85, bottom=0.18)
    figOcc.patch.set_facecolor('white')

    figOcc.savefig('./plots_' + save_title+"_nhits.png")
    

def esum_2D(ecal, hcal, fake_ecal, fake_hcal, nbinsX, nbinsY, name):
    

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(25, 6))
    
    vmaxH = hcal.max()
    #vminH = hcal.min()
    
    
    
    i1 = ax1.hist2d(hcal, ecal,bins=(nbinsX, nbinsY), 
               range = [[0,1500], [0,1500]], norm=LogNorm(vmax=vmaxH, vmin = 0.1), 
               cmap=plt.cm.Reds)
   

    i2 = ax2.hist2d(fake_hcal, fake_ecal,bins=(nbinsX, nbinsY), 
               range = [[0,1500], [0,1500]], norm=LogNorm(vmax=vmaxH, vmin = 0.1), 
               cmap=plt.cm.Reds)

    #i1 = ax1.hist2d(hcal, ecal,bins=(nbinsX, nbinsY), 
    #           range = [[0,1500], [0,1500]], norm=simple_norm(img, 'asinh', min_cut=vminH, max_cut=vmaxH),
    #           cmap=plt.cm.Reds)
   

    #i2 = ax2.hist2d(fake_hcal, fake_ecal,bins=(nbinsX, nbinsY), 
    #           range = [[0,1500], [0,1500]], norm=simple_norm(img, 'asinh', min_cut=vminH, max_cut=vmaxH),
    #           cmap=plt.cm.Reds)
    
    
    
    ax1.set_xlabel('HCAL E-Sum [MeV]', fontsize=18)
    ax1.set_ylabel('ECAL E-Sum [MeV]', fontsize=18)
    ax1.set_title('Real')
    
    ax2.set_xlabel('HCAL E-Sum [MeV]', fontsize=18)
    ax2.set_ylabel('ECAL E-Sum [MeV]', fontsize=18)
    ax2.set_title('Fake')
    
    fig.patch.set_facecolor('white')
    fig.colorbar(i1[3], ax=ax1)
    fig.colorbar(i2[3], ax=ax2)
    
    plt.savefig('./esum2D'+str(name)+'.png')
    
    
def plot_combinedSingleXZ(imageReal, imageFake, r):
    
    cmap = mpl.cm.viridis
    cmap.set_bad('white',1.)
   
    figExIm, (axExIm1, axExIm2) = plt.subplots(1, 2, figsize=(20, 6))
    
    
    
    # Y-Z Real
    imsumReal = np.sum(imageReal[r], axis=1)#+1.0
    im1 = axExIm1.imshow(imsumReal, filternorm=False, interpolation='none', cmap = cmap,
                       norm=mpl.colors.LogNorm(), origin='lower')
    figExIm.patch.set_facecolor('white')  
    axExIm1.set_xlabel('y [cells]', family='serif')
    axExIm1.set_ylabel('z [layers]', family='serif')
    figExIm.colorbar(im1)
    
    # Y-Z Fake
    imsumFake = np.sum(imageFake[r], axis=1)#+1.0
    im2 = axExIm2.imshow(imsumFake, filternorm=False, interpolation='none', cmap = cmap,
                       norm=mpl.colors.LogNorm(), origin='lower')
    figExIm.patch.set_facecolor('white')  
    axExIm2.set_xlabel('y [cells]', family='serif')
    axExIm2.set_ylabel('z [layers]', family='serif')
    #figExIm.colorbar(im2)
    
    
