import numpy as np
import matplotlib.pyplot as plt
import scipy.spatial.distance as dist

def getTotE(data, xbins, ybins, layers):
    data = np.reshape(data,[-1, layers*xbins*ybins])
    etot_arr = np.sum(data, axis=(1))
    return etot_arr


def getOcc(data, xbins, ybins, layers):
    data = np.reshape(data,[-1, layers*xbins*ybins])
    occ_arr = (data > 0.0).sum(axis=(1))
    return occ_arr


def getHitE(data, xbins, ybins, layers):
    ehit_arr = np.reshape(data,[data.shape[0]*xbins*ybins*layers])
    ehit_arr = ehit_arr[ehit_arr != 0.0]
    return ehit_arr

# calculates the center of gravity (as in CALICE) (0st moment)
def get0Moment(x):
    n, l = x.shape
    tiles = np.tile(np.arange(l), (n,1))
    y = x * tiles
    y = y.sum(1)
    y = y/x.sum(1)
    return y

def getLongProfile(data, xbins, ybins, layers):
    data = np.reshape(data,[-1, layers, xbins*ybins])
    etot_arr = np.sum(data, axis=(2))
    return etot_arr

def getRadialDistribution(data, xbins, ybins, layers):
    current = np.reshape(data,[-1, layers, xbins,ybins])
    current_sum = np.sum(current, axis=(0,1))
 
    r_list=[]
    phi_list=[]
    e_list=[]
    n_cent_x = (xbins-1)/2.0
    n_cent_y = (ybins-1)/2.0

    for n_x in np.arange(0, xbins):
        for n_y in np.arange(0, ybins):
            if current_sum[n_x,n_y] != 0.0:
                r = np.sqrt((n_x - n_cent_x)**2 + (n_y - n_cent_y)**2)
                r_list.append(r)
                phi = np.arctan((n_x - n_cent_x)/(n_y - n_cent_y))
                phi_list.append(phi)
                e_list.append(current_sum[n_x,n_y])
                
    r_arr = np.asarray(r_list)
    phi_arr = np.asarray(phi_list)
    e_arr = np.asarray(e_list)

    return r_arr, phi_arr, e_arr


def jsdHist(data_real, data_fake, nbins, minE, maxE, eph, debug=False):
    
    figSE = plt.figure(figsize=(6,6*0.77/0.67))
    axSE = figSE.add_subplot(1,1,1)


    pSEa = axSE.hist(data_real, bins=nbins, 
            weights=np.ones_like(data_real)/(float(len(data_real))), 
            histtype='step', color='black',
            range=[minE, maxE])
    pSEb = axSE.hist(data_fake, bins=nbins, 
            weights=np.ones_like(data_fake)/(float(len(data_fake))),
            histtype='step', color='red',
             range=[minE, maxE])

    frq1 = pSEa[0]
    frq2 = pSEb[0]

    JSD = dist.jensenshannon(frq1, frq2)    

    if (debug):
        plt.savefig('./jsd/esum_'+str(eph)+'.png')

    plt.close()
    
    if len(frq1) != len(frq2):
        print('ERROR JSD: Histogram bins are not matching!!')
    return JSD