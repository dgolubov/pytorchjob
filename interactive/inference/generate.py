import torch
import random
import sys
from torch.autograd import Variable
import numpy as np
import h5py
import torch.nn as nn
import matplotlib.pyplot as plt
sys.path.append('/home/jovyan/pytorchjob')
from models.generator import DCGAN_G
from models.generatorFull import Hcal_ecalEMB
import interactive.physics.basics as B    
import scipy.spatial.distance as dist


def make_shower(eph, Etrue, nEvents): 
    
    
    ngf = 32
    nz = 100
    batch_size = 500
    device = torch.device("cuda")
    ## LOAD ECAL GENERATOR
    mGenE = DCGAN_G(ngf, nz)
    mGenE = torch.nn.parallel.DataParallel(mGenE)
    exp='wganHCAL_p2critv4'
    #eph_ecal = 42

    gen_checkpointECAL = torch.load('/eos/user/e/eneren/experiments/' + exp + "_generatorE_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
    mGenE.load_state_dict(gen_checkpointECAL['model_state_dict'])
    mGenE.eval()
    #####

    ## LOAD HCAL GENERATOR
    mGenH = Hcal_ecalEMB(ngf, 32, nz).to(device)
    mGenH = torch.nn.parallel.DataParallel(mGenH)
    expH = 'wganHCAL_p2critv4'
    #expH = 'wganHCALv1'
    
    Tensor = torch.cuda.FloatTensor 
    
    
    gen_checkpointHCAL = torch.load('/eos/user/e/eneren/experiments/' + expH + "_generatorH_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
    mGenH.load_state_dict(gen_checkpointHCAL['model_state_dict'])
    mGenH.eval()
        
    fakeList = []
    fE_list = []
    fH_list = []
    for b in range(batch_size, nEvents + 1 , batch_size):
        
        print ("Generating of showers: current batch {}  .......".format(b))
        ##ECAL generate
        z = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz, 1, 1, 1))))
        E = torch.from_numpy(np.random.uniform(Etrue, Etrue, (batch_size,1))).float()

        ecal_shower = mGenE(z, E.view(-1, 1, 1, 1, 1)).detach()
        ecal_shower = ecal_shower.unsqueeze(1) 
        ####

        ## HCAL generate
        zH = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz))))
        hcal_shower = mGenH(zH, E, ecal_shower).detach()

        ecal_shower = ecal_shower.squeeze(1)
        comb = torch.cat((ecal_shower, hcal_shower),1)
       
        fakeList.append(comb.cpu().numpy())
        fE_list.append(ecal_shower.cpu().numpy())
        fH_list.append(hcal_shower.cpu().numpy())
    
    
    im = np.vstack(fakeList)
    imE = np.vstack(fE_list)
    imH = np.vstack(fH_list)
    
    return im, imE, imH




def make_shower_rECAL(eph, Etrue, nEvents): 
    
    
    ngf = 32
    nz = 100
    batch_size = 1000
    device = torch.device("cuda")
    ## LOAD REAL ECAL DATA
    f50 = h5py.File('/eos/user/e/eneren/scratch/50GeV75k.hdf5', 'r')
    #####

    
    ## LOAD HCAL GENERATOR
    mGenH = Hcal_ecalEMB(ngf, 32, nz, emb_size=32).to(device)
    mGenH = torch.nn.parallel.DataParallel(mGenH)
    expH = 'wganHCAL50GeV_v2'
    #expH = 'wganHCALv1'
    
    Tensor = torch.cuda.FloatTensor 
    
    
    gen_checkpointHCAL = torch.load('/eos/user/e/eneren/experiments/' + expH + "_generator_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
    mGenH.load_state_dict(gen_checkpointHCAL['model_state_dict'])
    mGenH.eval()
        
    fakeList = []
    fE_list = []
    fH_list = []
    for b in range(batch_size, nEvents + 1 , batch_size):
        
        print ("Generating of showers: current batch {}  .......".format(b))
        ##ECAL REAL
        s50E = f50['ecal/layers'][b-batch_size:b]
        ecal_shower = torch.from_numpy(s50E).float()
        ecal_shower = ecal_shower.unsqueeze(1)
        ecal_shower = ecal_shower.cuda()
        E = torch.from_numpy(np.random.uniform(Etrue, Etrue, (batch_size,1))).float()
        ####

        ## HCAL generate
        zH = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz))))
        hcal_shower = mGenH(zH, E, ecal_shower).detach()

        ecal_shower = ecal_shower.squeeze(1)
        comb = torch.cat((ecal_shower, hcal_shower),1)
       
        fakeList.append(comb.cpu().numpy())
        fE_list.append(ecal_shower.cpu().numpy())
        fH_list.append(hcal_shower.cpu().numpy())
    
    
    im = np.vstack(fakeList)
    imE = np.vstack(fE_list)
    imH = np.vstack(fH_list)
    
    return im, imE, imH


def fid_scan_rECAL(showers, eph_start, eph_end):
    
    ngf = 32
    nz = 100
    batch_size = 1000
    device = torch.device("cuda")
    ## LOAD REAL ECAL DATA
    f50 = h5py.File('/eos/user/e/eneren/scratch/50GeV75k.hdf5', 'r')
    #####

    ## LOAD HCAL GENERATOR
    mGenH = Hcal_ecalEMB(ngf, 32, nz, emb_size=32).to(device)
    mGenH = nn.parallel.DataParallel(mGenH)
    expH = 'wganHCAL50GeV_v2'
    
    Tensor = torch.cuda.FloatTensor 
    eph_list = list(range(eph_start,eph_end,1))
    
    esum_mean = []
    esum_down = []
    esum_up = []
    for eph in eph_list:
        gen_checkpointHCAL = torch.load('/eos/user/e/eneren/experiments/' + expH + "_generator_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
        mGenH.load_state_dict(gen_checkpointHCAL['model_state_dict'])
        mGenH.eval()
        
        print ("Epoch: " , eph)
        jsd = []
        for Etrue in [50]:

            ##ECAL real
            ##ECAL REAL
            s50E = f50['ecal/layers'][:1000]
            ecal_shower = torch.from_numpy(s50E).float()
            ecal_shower = ecal_shower.unsqueeze(1)
            ecal_shower = ecal_shower.cuda()
            E = torch.from_numpy(np.random.uniform(Etrue, Etrue, (batch_size,1))).float()
            ####
            ####
            
            ## HCAL generate
            zH = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz))))
            hcal_shower = mGenH(zH, E, ecal_shower).detach()

            ecal_shower = ecal_shower.squeeze(1)
            comb = torch.cat((ecal_shower, hcal_shower),1)

            esumFake = B.getTotE(comb.cpu().numpy(), 30, 30, 78)
            esumReal = showers[str(Etrue)+'F']

            

            jsd.append(B.jsdHist(esumReal, esumFake, 50, 0, 2500, eph, debug=False))
        
        #m = (jsd[0] + jsd[1] + jsd[2]) / 3.0
        m = jsd[0]
        esum_mean.append(m)
        esum_down.append(m - min(jsd))
        esum_up.append(max(jsd) - m)
        
    
        print ("Epoch: ", jsd)
        

    
    # Plotting
    # Energy sum with average and spread
    plt.figure(figsize=(12,4), facecolor='none', dpi=400)
    plt.scatter(eph_list, esum_mean, marker = 'x', color='red', label='Average Energy sum')
    #plt.scatter(epoch_list, epoch_matrix_median[:,0], marker = '+', color='blue', label='Median Energy sum area difference')
    plt.errorbar(eph_list, esum_mean, yerr=[esum_down, esum_up], color='black', label='Min-Max interval', linestyle='None')
    #plt.errorbar(epoch_list, epoch_matrix_mean[:,0], yerr=epoch_matrix_std[:,0], color='green', label='Standard deviation', linestyle='None', capsize=3.0)


    plt.legend(loc='upper right', fontsize=10)
    plt.xlabel('epoch', fontsize=14)
    plt.ylabel('JS difference', fontsize=16)
    plt.ylim(0.1,1.0)
    
    plt.savefig('/eos/user/e/eneren/plots/sweep_min_max__'+str(eph_start)+'__'+str(eph_end)+'.png')
    
    
    
    
def fid_scan(showers, nevents, eph_start, eph_end):
    
    ngf = 32
    nz = 100
    batch_size = 500
    device = torch.device("cuda")
    ## LOAD ECAL GENERATOR
    mGenE = DCGAN_G(ngf, nz)
    mGenE = torch.nn.parallel.DataParallel(mGenE)
    exp='wganHCAL_p2critv4'
    #eph_ecal = 694

    
    #####


    ## LOAD HCAL GENERATOR
    mGenH = Hcal_ecalEMB(ngf, 32, nz).to(device)
    mGenH = nn.parallel.DataParallel(mGenH)
    expH = 'wganHCAL_p2critv4'
    
    Tensor = torch.cuda.FloatTensor 
    Etrue = 50
    eph_list = list(range(eph_start,eph_end,1))
    esum_mean = []
    
    for eph in eph_list:
        
        #esum_mean = []
        #esum_down = []
        #esum_up = []
        
        gen_checkpointHCAL = torch.load('/eos/user/e/eneren/experiments/' + expH + "_generatorH_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
        mGenH.load_state_dict(gen_checkpointHCAL['model_state_dict'])
        mGenH.eval()

        gen_checkpointECAL = torch.load('/eos/user/e/eneren/experiments/' + exp + "_generatorE_"+ str(eph) + ".pt", map_location=torch.device('cuda'))
        mGenE.load_state_dict(gen_checkpointECAL['model_state_dict'])
        mGenE.eval()
        
        print ("Current Epoch: " , eph)
        
        
        fakeL = []
        for b in range(batch_size, nevents + 1, batch_size):
            ##ECAL generate
            z = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz, 1, 1, 1))))
            E = torch.from_numpy(np.random.uniform(Etrue, Etrue, (batch_size,1))).float()

            ecal_shower = mGenE(z, E.view(-1, 1, 1, 1, 1)).detach()
            ecal_shower = ecal_shower.unsqueeze(1) 
            ####


            ## HCAL generate
            zH = Variable(Tensor(np.random.uniform(-1, 1, (batch_size, nz))))
            hcal_shower = mGenH(zH, E, ecal_shower).detach()
            ####


            ecal_shower = ecal_shower.squeeze(1)
            ## MIT CUT
            ecal_shower[ ecal_shower < 0.1] = 0.0
            hcal_shower[ hcal_shower < 0.25] = 0.0
            
            comb = torch.cat((ecal_shower, hcal_shower),1)

            esumFull = B.getTotE(comb.cpu().numpy(), 78, 30, 30)
            fakeL.append(esumFull)
        
        
        esumFake = np.asarray(fakeL)

        
        
        jsd = B.jsdHist(showers[str(Etrue)+'F'], esumFake.flatten(), 50, 0, 2500, eph, debug=False)

        #m = (jsd[0] + jsd[1] + jsd[2]) / 3.0
        #m = jsd
        esum_mean.append(jsd)
        #esum_down.append(m - min(jsd))
        #esum_up.append(max(jsd) - m)


        print ("JSD score: ", jsd)
        
    print("Best: ", min(esum_mean))
    # Plotting
    # Energy sum with average and spread
    plt.figure(figsize=(12,4), facecolor='none', dpi=400)
    plt.scatter(eph_list, esum_mean, marker = 'x', color='red', label='Energy sum')
    #plt.scatter(epoch_list, epoch_matrix_median[:,0], marker = '+', color='blue', label='Median Energy sum area difference')
    #plt.errorbar(eph_list, esum_mean, yerr=[esum_down, esum_up], color='black', label='Min-Max interval', linestyle='None')
    #plt.errorbar(epoch_list, epoch_matrix_mean[:,0], yerr=epoch_matrix_std[:,0], color='green', label='Standard deviation', linestyle='None', capsize=3.0)


    plt.legend(loc='upper right', fontsize=10)
    plt.xlabel('epoch', fontsize=14)
    plt.ylabel('JS difference', fontsize=16)
    plt.ylim(0.1,1.0)
    
    plt.savefig('/eos/user/e/eneren/plots/sweep_min_max__'+str(eph_start)+'__'+str(eph_end)+'.png')