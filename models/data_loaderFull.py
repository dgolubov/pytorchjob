
import numpy as np
import torch
import os
import h5py
from torch.utils import data



class HDF5Dataset(data.Dataset):
    def __init__(self, file_path, train_size, transform=None):
        super().__init__()
        self.file_path = file_path
        self.transform = transform
        self.hdf5file = h5py.File(self.file_path, 'r')
        
        if train_size > self.hdf5file['ecal']['layers'].shape[0]-1:
            self.train_size = self.hdf5file['ecal']['layers'].shape[0]-1
        else:
            self.train_size = train_size
            
        
    def __len__(self):
        return self.train_size
             
    def __getitem__(self, index):
        # get ECAL part
        x = self.get_data(index)
        x = torch.from_numpy(x).float()
        
        ## get HCAL part
        y = self.get_data_hcal(index)
        y = torch.from_numpy(y).float()
        
        e = torch.from_numpy(self.get_energy(index))
        
        return x, y, e
    
    def get_data(self, i):
        return self.hdf5file['ecal']['layers'][i]

    def get_data_hcal(self, i):
        return self.hdf5file['hcal']['layers'][i]
    
    def get_energy(self, i):
        return self.hdf5file['ecal']['energy'][i]


