import torch
import torch.nn as nn
import torch.nn.parallel
import torch.nn.functional as F


class CriticEMB(nn.Module):
    def __init__(self, isize_1=30, isize_2=48, nc=2, ndf=64, size_embed=64):
        super(CriticEMB, self).__init__()    
        self.ndf = ndf
        self.isize_1 = isize_1
        self.isize_2 = isize_2
        self.nc = nc
        self.size_embed = size_embed
        self.conv1_bias = False

      
        
        # ECAL component of convolutions
        # Designed for input 30*30*30
        self.conv_ECAL_1 = torch.nn.Conv3d(1, ndf, kernel_size=(2,2,2), stride=(1,1,1), padding=0, bias=False)
        self.ln_ECAL_1 = torch.nn.LayerNorm([29,29,29])
        self.conv_ECAL_2 = torch.nn.Conv3d(ndf, ndf, kernel_size=2, stride=(2,2,2), padding=0, bias=False)
        self.ln_ECAL_2 = torch.nn.LayerNorm([14,14,14])
        self.conv_ECAL_3 = torch.nn.Conv3d(ndf, ndf, kernel_size=4, stride=(2,2,2), padding=(1,1,1), bias=False)
        # output [batch_size, ndf, 7, 7, 7]
        
        # HCAL component of convolutions
        # Designed for input 48*30*30
        self.conv_HCAL_1 = torch.nn.Conv3d(1, ndf, kernel_size=2, stride=(2,1,1), padding=(5,0,0), bias=False)
        self.ln_HCAL_1 = torch.nn.LayerNorm([29,29,29])
        self.conv_HCAL_2 = torch.nn.Conv3d(ndf, ndf, kernel_size=2, stride=(2,2,2), padding=0, bias=False)
        self.ln_HCAL_2 = torch.nn.LayerNorm([14,14,14])
        self.conv_HCAL_3 = torch.nn.Conv3d(ndf, ndf, kernel_size=4, stride=(2,2,2), padding=(1,1,1), bias=False)
        # output [batch_size, ndf, 7, 7, 7]
        
        # alternative structure for 48*25*25 HCAL
        #self.conv_HCAL_1 = torch.nn.Conv3d(1, ndf, kernel_size=2, stride=(2,1,1), padding=0, bias=False)
        #self.ln_HCAL_1 = torch.nn.LayerNorm([24,24,24])
        #self.conv_HCAL_2 = torch.nn.Conv3d(ndf, ndf, kernel_size=2, stride=(2,2,2), padding=0, bias=False)
        #self.ln_HCAL_2 = torch.nn.LayerNorm([12,12,12])
        #self.conv_HCAL_3 = torch.nn.Conv3d(ndf, ndf, kernel_size=4, stride=(2,2,2), padding=(1,1,1), bias=False)
        

        self.conv_lin_ECAL = torch.nn.Linear(7*7*7*ndf, size_embed) 
        self.conv_lin_HCAL = torch.nn.Linear(7*7*7*ndf, size_embed) 
        
        self.econd_lin = torch.nn.Linear(1, size_embed) # label embedding

        self.fc1 = torch.nn.Linear(size_embed*3, size_embed*2)  # 3 components after cat
        self.fc2 = torch.nn.Linear(size_embed*2,  size_embed)
        self.fc3 = torch.nn.Linear(size_embed, 1)


    def forward(self, img_ECAL, img_HCAL, E_true):
        batch_size = img_ECAL.size(0)
        # input: img_ECAL = [batch_size, 1, 30, 30, 30]
        #        img_HCAL = [batch_size, 1, 48, 30, 30] 
        
        # ECAL 
        x_ECAL = F.leaky_relu(self.ln_ECAL_1(self.conv_ECAL_1(img_ECAL)), 0.2)
        x_ECAL = F.leaky_relu(self.ln_ECAL_2(self.conv_ECAL_2(x_ECAL)), 0.2)
        x_ECAL = F.leaky_relu(self.conv_ECAL_3(x_ECAL), 0.2)
        x_ECAL = x_ECAL.view(-1, self.ndf*7*7*7)
        x_ECAL = F.leaky_relu(self.conv_lin_ECAL(x_ECAL), 0.2)
        
        # HCAL
        x_HCAL = F.leaky_relu(self.ln_HCAL_1(self.conv_HCAL_1(img_HCAL)), 0.2)
        x_HCAL = F.leaky_relu(self.ln_HCAL_2(self.conv_HCAL_2(x_HCAL)), 0.2)
        x_HCAL = F.leaky_relu(self.conv_HCAL_3(x_HCAL), 0.2)
        x_HCAL = x_HCAL.view(-1, self.ndf*7*7*7)
        x_HCAL = F.leaky_relu(self.conv_lin_HCAL(x_HCAL), 0.2)        
        
        x_E = F.leaky_relu(self.econd_lin(E_true), 0.2)
        
        xa = torch.cat((x_ECAL, x_HCAL, x_E), 1)
        
        xa = F.leaky_relu(self.fc1(xa), 0.2)
        xa = F.leaky_relu(self.fc2(xa), 0.2)
        xa = self.fc3(xa)
        
        return xa ### flattens