FROM pytorch/pytorch:1.9.0-cuda10.2-cudnn7-runtime

ENV NB_USER jovyan
ENV NB_UID 1000
ENV HOME /home/$NB_USER

RUN apt-get -qq update && \
    apt-get -yqq install libpam-krb5 krb5-user && \
    apt-get -yqq clean && \
    apt-get install -y --no-install-recommends \
        ca-certificates bash-completion tar less \
        python-pip python-setuptools build-essential python-dev \
        python3-pip python3-wheel && \
    rm -rf /var/lib/apt/lists/*

# create user and set required ownership
RUN useradd -M -s /bin/bash -N -u ${NB_UID} ${NB_USER} \
     && mkdir -p ${HOME} \
    && chown -R ${NB_USER}:users ${HOME} \
    && chown -R ${NB_USER}:users /usr/local/bin 

RUN  mkdir -p ${HOME}/models \
    && pip install h5py pyflakes comet_ml && export MKL_SERVICE_FORCE_INTEL=1

WORKDIR ${HOME}

ADD wgan.py ${HOME}/wgan.py
ADD wganHCAL.py ${HOME}/wganHCAL.py
ADD wgan_ECAL_HCAL_3crit.py ${HOME}/wgan_ECAL_HCAL_3crit.py

COPY ./models/* ${HOME}/models/
COPY docker/krb5.conf /etc/krb5.conf

RUN mkdir -p /etc/sudoers.d && echo "jovyan ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/jovyan
RUN echo 'PS1="${debian_chroot:+($debian_chroot)}@\h:\w\$ "' >> /etc/bash.bashrc

USER $NB_USER


ENTRYPOINT ["/bin/bash"]
